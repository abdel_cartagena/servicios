@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    EDITAR SERVICIO #{{ $servicio->id }}
                </div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('servicios.update', $servicio) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre</label>
                            <input value="{{ $servicio->nombre }}" type="text" class="form-control" name="nombre">
                        </div>
                        <div class="form-group">
                            <label >Imagen</label>
                            <input type="file" class="form-control" name="imagen">
                        </div>
                        <div class="form-group">
                            <label >Tipo servicio</label>
                            <select class="form-control" name="tipo_servicio" value="{{ old('tipo_servicio', $servicio->tipo_servicio) }}">
                                
                                @if ($servicio->tipo_servicio == 'basico')
                                    <option value="basico" selected>Basico</option>
                                    <option value="avanzado">Avanzado</option>
                                @else
                                    <option value="basico">Basico</option>
                                    <option value="avanzado" selected>Avanzado</option>
                                @endif
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Fecha inicio</label>
                            <input value="{{ $servicio->fecha_inicio }}" type="date" class="form-control" name="fecha_inicio">
                        </div>
                        <div class="form-group">
                            <label >Fecha fin</label>
                            <input value="{{ $servicio->fecha_fin }}" type="date" class="form-control" name="fecha_fin">
                        </div>
                        <div class="form-group">
                            <label >Observaciones</label>
                            <textarea class="form-control" name="observaciones" rows="2">{{ $servicio->observaciones }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" style="float: right;">Guardar</button>
                        <a href="{{ route('servicios.store') }}" type="button" class="btn btn-default" style="float: right; margin-right: 10px;">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
