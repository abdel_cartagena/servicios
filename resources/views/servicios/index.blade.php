@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    SERVICIOS
                    <a href="{{ route('servicios.create') }}" type="button" class="btn btn-primary" style="float: right; margin-top: -7px;">
                        Crear Servicio
                    </a>
                </div>

                <div class="panel-body">

                    <table id="servicios_table" class="display">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Imagen</th>
                                <th>Tipo servicio</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th>Observaciones</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($servicios as $servicio)
                                <tr>
                                    <td>{{ $servicio->nombre }}</td>
                                    <td>
                                        @if (count($servicio->imagen) > 0)
                                            <img src="{{ asset("storage/{$servicio->imagen[0]->imagen_url}") }}" alt=""/>
                                        @endif
                                    </td>
                                    <td>{{ $servicio->tipo_servicio }}</td>
                                    <td>{{ $servicio->fecha_inicio }}</td>
                                    <td>{{ $servicio->fecha_fin }}</td>
                                    <td>{{ $servicio->observaciones }}</td>
                                    <td>
                                        <a href="{{ route('servicios.edit', $servicio) }}" type="button" class="btn btn-warning btn-sm">Editar</a>
                                        <form method="POST" action="{{ route('servicios.destroy', $servicio) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#servicios_table').DataTable();
    });
</script>
@endsection
