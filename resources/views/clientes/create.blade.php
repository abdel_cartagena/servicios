@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Agregar servicio
                </div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('clientes.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label >Nombre</label>
                            <input type="text" class="form-control" name="nombre">
                        </div>
                        <div class="form-group">
                            <label >Imagen</label>
                            <input type="file" class="form-control" name="imagen">
                        </div>
                        <div class="form-group">
                            <label >Tipo servicio</label>
                            <select class="form-control" name="tipo_servicio[]" multiple="multiple">
                                @foreach ($servicios as $servicio)
                                    <option value="{{ $servicio->id }}">{{ $servicio->nombre }} - {{ $servicio->tipo_servicio }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Correo</label>
                            <input type="mail" class="form-control" name="correo">
                        </div>
                        <div class="form-group">
                            <label >Cédula</label>
                            <input type="text" class="form-control" name="cedula">
                        </div>
                        <div class="form-group">
                            <label >Telefono</label>
                            <input type="text" class="form-control" name="telefono">
                        </div>
                        <div class="form-group">
                            <label >Observaciones</label>
                            <textarea class="form-control" name="observaciones" rows="2"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" style="float: right;">Guardar</button>
                        <a href="{{ route('clientes.index') }}" type="button" class="btn btn-danger" style="float: right; margin-right: 10px;">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
