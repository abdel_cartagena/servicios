@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    CLIENTES
                    <a href="{{ route('clientes.create') }}" type="button" class="btn btn-primary" style="float: right; margin-top: -7px;">
                        Crear Cliente
                    </a>
                </div>

                <div class="panel-body">

                    <table id="clientes_table" class="display">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Imagen</th>
                                <th>Correo</th>
                                <th>Cedula</th>
                                <th>Telefono</th>
                                <th>Observaciones</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clientes as $cliente)
                                <tr>
                                    <td>{{ $cliente->nombre }}</td>
                                    <td>
                                        @if (count($cliente->imagen) > 0)
                                            <img src="{{ asset("storage/{$cliente->imagen[0]->imagen_url}") }}" alt=""/>
                                        @endif
                                    </td>
                                    <td>{{ $cliente->correo }}</td>
                                    <td>{{ $cliente->cedula }}</td>
                                    <td>{{ $cliente->telefono }}</td>
                                    <td>{{ $cliente->observaciones }}</td>
                                    <td>
                                        <a href="{{ route('clientes.show', $cliente) }}" type="button" class="btn btn-primary btn-sm">Ver</a>
                                        <a href="{{ route('clientes.edit', $cliente) }}" type="button" class="btn btn-warning btn-sm">Editar</a>
                                        <form method="POST" action="{{ route('clientes.destroy', $cliente) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#clientes_table').DataTable();
    });
</script>
@endsection
