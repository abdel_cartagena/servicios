@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    EDITAR CLIENTE #{{ $cliente->id }}
                </div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('clientes.update', $cliente) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre</label>
                            <input value="{{ $cliente->nombre }}" type="text" class="form-control" name="nombre">
                        </div>
                        <div class="form-group">
                            <label >Imagen</label>
                            <input type="file" class="form-control" name="imagen">
                        </div>
                        <div class="form-group">
                            <label >Tipo servicio</label>
                            <select class="form-control" name="tipo_servicio[]" multiple="multiple">
                                @foreach ($servicios as $servicio)
                                    <option value="{{ $servicio->id }}">{{ $servicio->nombre }} - {{ $servicio->tipo_servicio }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Correo</label>
                            <input value="{{ $cliente->correo }}" type="text" class="form-control" name="correo">
                        </div>
                        <div class="form-group">
                            <label >Cédula</label>
                            <input value="{{ $cliente->cedula }}" type="text" class="form-control" name="cedula">
                        </div>
                        <div class="form-group">
                            <label >Telefono</label>
                            <input value="{{ $cliente->telefono }}" type="text" class="form-control" name="telefono">
                        </div>
                        <div class="form-group">
                            <label >Observaciones</label>
                            <textarea class="form-control" name="observaciones" rows="2">{{ $cliente->observaciones }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" style="float: right;">Guardar</button>
                        <a href="{{ route('clientes.store') }}" type="button" class="btn btn-default" style="float: right; margin-right: 10px;">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
