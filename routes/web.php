<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//CLIENTE
Route::get('/clientes', 'ClientesController@index')->name('clientes.index');
Route::post('/clientes', 'ClientesController@store')->name('clientes.store');
Route::get('/clientes/create', 'ClientesController@create')->name('clientes.create');
Route::get('/clientes/ver/{id}', 'ClientesController@show')->name('clientes.show');
Route::get('/clientes/editar/{id}', 'ClientesController@edit')->name('clientes.edit');
Route::patch('/clientes/{id}', 'ClientesController@update')->name('clientes.update');
Route::delete('/clientes/{id}', 'ClientesController@destroy')->name('clientes.destroy');
//SERVICIOS
Route::get('/servicios', 'ServiciosController@index')->name('servicios.index');
Route::post('/servicios', 'ServiciosController@store')->name('servicios.store');
Route::get('/servicios/create', 'ServiciosController@create')->name('servicios.create');
Route::get('/servicios/{id}', 'ServiciosController@show')->name('servicios.show');
Route::get('/servicios/editar/{id}', 'ServiciosController@edit')->name('servicios.edit');
Route::patch('/servicios/{id}', 'ServiciosController@update')->name('servicios.update');
Route::delete('/servicios/{id}', 'ServiciosController@destroy')->name('servicios.destroy');