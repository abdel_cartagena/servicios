<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Clientes;
use App\Imagenes;
use App\Servicios;
use App\ClienteHasServicio;

class ClientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clientes.index', ['clientes' => Clientes::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create', ['servicios' => Servicios::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nuevoCliente = Clientes::create([
            'nombre' => $request->get('nombre'),
            'correo' => $request->get('correo'),
            'cedula' => $request->get('cedula'),
            'telefono' => $request->get('telefono'),
            'observaciones' => $request->get('observaciones')
        ]);

        $nuevoCliente = Clientes::first();

        if($request->file('imagen')){
            $imagenUrl = Storage::put('imagen', $request->file('imagen'), 'public');
            $nuevoCliente->imagen()->create([
                'imagen_url' => $imagenUrl
            ]);
        }

        foreach ($request->get('tipo_servicio') as $key => $value) {
            $nuevoCliente->servicios()->create([
                'servicio_id' => $value,
            ]);
        }

        return view('clientes.index', ['clientes' => Clientes::get()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd(ClienteHasServicio::where('cliente_id', $id)->first()->servicio);
        return view('clientes.show', [
            'cliente' => Clientes::find($id),
            'servicios' => ClienteHasServicio::where('cliente_id', $id)->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('clientes.edit', [
            'servicios' => Servicios::get(),
            'cliente' => Clientes::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Clientes::find($id);

        $cliente->update([
            'nombre' => $request->get('nombre'),
            'correo' => $request->get('correo'),
            'cedula' => $request->get('cedula'),
            'telefono' => $request->get('telefono'),
            'observaciones' => $request->get('observaciones')
        ]);

        if($request->file('imagen')){
            if(count($cliente->imagen) > 0){
                Imagenes::find($cliente->imagen[0]->id)->delete();
            }
            $imagenUrl = Storage::put('imagen', $request->file('imagen'), 'public');
            $cliente->imagen()->create([
                'imagen_url' => $imagenUrl
            ]);
        }

        ClienteHasServicio::where('cliente_id', $id)->delete();

        if(count($request->get('tipo_servicio')) > 0){
            foreach ($request->get('tipo_servicio') as $key => $value) {
                $cliente->servicios()->create([
                    'servicio_id' => $value,
                ]);
            }
        }
        
        return view('clientes.index', ['clientes' => Clientes::get()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClienteHasServicio::where('cliente_id', $id)->delete();
        Clientes::find($id)->delete();
        return view('clientes.index', [
            'clientes' => Clientes::get()
        ]);
    }
}
