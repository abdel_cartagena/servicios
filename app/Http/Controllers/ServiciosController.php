<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Imagenes;
use App\Servicios;

class ServiciosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Servicios::get()[1]->imagen);
        return view('servicios.index', ['servicios' => Servicios::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nuevoServicio = Servicios::create([
            'nombre' => $request->get('nombre'),
            'tipo_servicio' => $request->get('tipo_servicio'),
            'fecha_inicio' => $request->get('fecha_inicio'),
            'fecha_fin' => $request->get('fecha_fin'),
            'observaciones' => $request->get('observaciones')
        ]);

        if($request->file('imagen')){
            $imagenUrl = Storage::put('imagen', $request->file('imagen'), 'public');
            $nuevoServicio->imagen()->create([
                'imagen_url' => $imagenUrl
            ]);
        }

        return view('servicios.index', ['servicios' => Servicios::get()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('servicios.show', ['servicios' => Servicios::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('servicios.edit', ['servicio' => Servicios::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicio = Servicios::find($id);
        
        $servicio->update([
            'nombre' => $request->get('nombre'),
            'tipo_servicio' => $request->get('tipo_servicio'),
            'fecha_inicio' => $request->get('fecha_inicio'),
            'fecha_fin' => $request->get('fecha_fin'),
            'observaciones' => $request->get('observaciones')
        ]);

        if($request->file('imagen')){
            Imagenes::find($servicio->imagen[0]->id)->delete();
            $imagenUrl = Storage::put('imagen', $request->file('imagen'), 'public');
            $servicio->imagen()->create([
                'imagen_url' => $imagenUrl
            ]);
        }
        
        return view('servicios.index', ['servicios' => Servicios::get()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Servicios::find($id)->delete();
        return view('servicios.index', ['servicios' => Servicios::get()]);
    }
}
