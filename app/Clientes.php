<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = [
        'id',
        'nombre',
        'imagen',
        'correo',
        'cedula',
        'telefono',
        'observaciones',
    ];

    public function imagen()
    {
        return $this->morphMany('App\Imagenes', 'imagen');
    }

    public function servicios()
    {
        return $this->hasMany('App\ClienteHasServicio', 'cliente_id', 'id');
    }

    public function getMorphClass()
    {
        return 'cliente';
    }
}
