<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteHasServicio extends Model
{
    protected $fillable = [
        'id',
        'cliente_id',
        'servicio_id',
    ];

    public function servicio()
    {
        return $this->belongsTo('App\Servicios', 'servicio_id', 'id');
    }
}
