<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios extends Model
{
    protected $fillable = [
        'id',
        'nombre',
        'tipo_servicio',
        'fecha_inicio',
        'fecha_fin',
        'observaciones',
    ];

    public function imagen()
    {
        return $this->morphMany('App\Imagenes', 'imagen');
    }

    public function getMorphClass()
    {
        return 'servicio';
    }
}
